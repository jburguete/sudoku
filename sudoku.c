#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG 0

enum ErrorCode
{
  ERROR_CODE_NONE = 0,
  ERROR_CODE_BAD_ARGUMENTS_NUMBER,
  ERROR_CODE_NO_INPUT_FILE,
  ERROR_CODE_BAD_INPUT_FILE
};

const char *error_msg;
unsigned int max_allowed_deep;

static inline int
value (unsigned int stat[81])
{
  unsigned int i, v;
  for (i = v = 0; i < 81; ++i)
    v += stat[i];
  if (v == 405)
    return 1;
  return 0;
}

void
print (unsigned int stat[81])
{
  unsigned int i, j, k;
  for (i = 0; i < 9; ++i)
    {
      k = 9 * i;
      for (j = 0; j < 8; ++j)
        printf ("%u ", stat[k + j]);
      printf ("%u\n", stat[k + j]);
    }
  for (i = j = 0; i < 81; ++i)
    if (!stat[i])
      ++j;
  if (!j && value (stat))
    puts ("SOLVED!");
}

static inline void
get_index (unsigned int position, unsigned int *row, unsigned int *column,
           unsigned int *box)
{
  *row = position / 9;
  *column = position % 9;
  *box = 3 * (*row / 3) + *column / 3;
}

static inline int
init (unsigned int stat[81], unsigned int lr[9][10], unsigned int lc[9][10],
      unsigned int lb[9][10])
{
  int error_code;
  unsigned int i, j, k, m, n;

  // check for repeated numbers in rows
  for (i = 0; i < 9; ++i)
    {
      for (j = 0; j < 10; ++j)
        lr[i][j] = 0;
      for (j = 0, k = 9 * i; j < 9; ++j, ++k)
        ++lr[i][stat[k]];
      for (j = 1; j < 10; ++j)
        if (lr[i][j] > 1)
          {
            error_msg = "Bad input file (repeated number in row)";
            error_code = ERROR_CODE_BAD_INPUT_FILE;
            goto exit1;
          }
    }

  // check for repeated numbers in columns
  for (i = 0; i < 9; ++i)
    {
      for (j = 0; j < 10; ++j)
        lc[i][j] = 0;
      for (j = 0, k = i; j < 9; ++j, k += 9)
        ++lc[i][stat[k]];
      for (j = 1; j < 10; ++j)
        if (lc[i][j] > 1)
          {
            error_msg = "Bad input file (repeated number in column)";
            error_code = ERROR_CODE_BAD_INPUT_FILE;
            goto exit1;
          }
    }

  // check for repeated numbers in boxes
  for (i = 0; i < 9; ++i)
    for (j = 0; j < 10; ++j)
      lb[i][j] = 0;
  for (i = 0; i < 9; ++i)
    {
      for (j = 0; j < 3; ++j)
        for (k = 0; k < 3; ++k)
          {
            m = 3 * (i / 3) + j;
            n = 9 * i + 3 * j + k;
            ++lb[m][stat[n]];
          }
    }
  for (i = 0; i < 9; ++i)
    for (j = 1; j < 10; ++j)
      if (lb[i][j] > 1)
        {
          error_msg = "Bad input file (repeated number in box)";
          error_code = ERROR_CODE_BAD_INPUT_FILE;
          goto exit1;
        }
  error_code = ERROR_CODE_NONE;

  // exit
exit1:
  return error_code;
}

static inline int
cells (unsigned int stat[81], unsigned int lr[9][10], unsigned int lc[9][10],
       unsigned int lb[9][10])
{
  unsigned int i, j, k, l, r, c, b;
  for (i = 0; i < 81; ++i)
    if (!stat[i])
      {
        // get row, column and box
        get_index (i, &r, &c, &b);
        for (j = 1; j < 10; ++j)
          {
            // check row
            if (!lr[r][j])
              {
                // check column and box
                l = 0;
                if (!lc[c][j] && !lb[b][j])
                  for (k = 1; k < 10; ++k)
                    if (!lr[r][k] && (lc[c][k] || lb[b][k]))
                      ++l;
                if (l == lr[r][0] - 1)
                  {
#if DEBUG
                    printf ("%u in row %u column %u (cel-r)\n",
                            j, r + 1, c + 1);
#endif
                    stat[i] = j;
                    return 1;
                  }
              }
            // check column
            if (!lc[c][j])
              {
                // check row and box
                l = 0;
                if (!lr[r][j] && !lb[b][j])
                  for (k = 1; k < 10; ++k)
                    if (!lc[c][k] && (lr[r][k] || lb[b][k]))
                      ++l;
                if (l == lc[c][0] - 1)
                  {
#if DEBUG
                    printf ("%u in row %u column %u (cel-c)\n",
                            j, r + 1, c + 1);
#endif
                    stat[i] = j;
                    return 1;
                  }
              }
            // check box
            if (!lb[b][j])
              {
                // check row and column 
                l = 0;
                if (!lr[r][j] && !lc[c][j])
                  for (k = 1; k < 10; ++k)
                    if (!lb[b][k] && (lr[r][k] || lc[c][k]))
                      ++l;
                if (l == lb[b][0] - 1)
                  {
#if DEBUG
                    printf ("%u in row %u column %u (cel-b)\n",
                            j, r + 1, c + 1);
#endif
                    stat[i] = j;
                    return 1;
                  }
              }
          }
      }
  return 0;
}

static inline unsigned int
count (unsigned int l[10][81])
{
  unsigned int i, j, c;
  for (c = 0, i = 1; i < 10; ++i)
    for (j = 0; j < 81; ++j)
      c += l[i][j];
  return c;
}

int
discards (unsigned int stat[81])
{
  unsigned int l[10][81];
  unsigned int i, j, k, m, r, c, b, v, v1, v2, v3, s;
  for (i = 1; i < 10; ++i)
    for (j = 0; j < 81; ++j)
      l[i][j] = 0;
  for (i = 0; i < 81; ++i)
    {
      if (!stat[i])
        continue;
      for (j = 1; j < 10; ++j)
        l[j][i] = 1;
      v = stat[i];
      get_index (i, &r, &c, &b);
      for (j = 0; j < 9; ++j)
        l[v][j * 9 + c] = l[v][r * 9 + j] = 1;
      for (j = 0; j < 3; ++j)
        for (k = 0; k < 3; ++k)
          l[v][27 * (b / 3) + 3 * (b % 3) + 9 * j + k] = 1;
    }
#if DEBUG
  for (j = 1; j < 10; ++j)
    {
      printf ("%u\n", j);
      print (&l[j][0]);
    }
#endif
bucle:
  s = count (l);
  // check values
  for (i = 1; i < 10; ++i)
    {
      // check rows
      for (j = 0; j < 9; ++j)
        {
          for (k = v = 0; k < 9; ++k)
            v += l[i][9 * j + k];
          if (v == 8)
            for (k = 0; k < 9; ++k)
              if (!l[i][9 * j + k])
                {
#if DEBUG
                  printf ("%u in row %u column %u (row)\n", i, j + 1, k + 1);
#endif
                  stat[9 * j + k] = i;
                  return 1;
                }
          if (v == 6 || v == 7)
            {
              for (k = v1 = 0; k < 3; ++k)
                v1 += l[i][9 * j + k];
              for (v2 = 0; k < 6; ++k)
                v2 += l[i][9 * j + k];
              for (v3 = 0; k < 9; ++k)
                v3 += l[i][9 * j + k];
              if (v2 == 3 && v3 == 3)
                switch (j % 3)
                  {
                  case 0:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 9 + m] =
                        l[i][27 * (j / 3) + 18 + m] = 1;
#if DEBUG
                    printf ("row230 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 1:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + m] = l[i][27 * (j / 3) + 18 + m] = 1;
#if DEBUG
                    printf ("row231 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 2:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + m] = l[i][27 * (j / 3) + 9 + m] = 1;
#if DEBUG
                    printf ("row232 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                  }
              else if (v1 == 3 && v3 == 3)
                switch (j % 3)
                  {
                  case 0:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 12 + m] =
                        l[i][27 * (j / 3) + 21 + m] = 1;
#if DEBUG
                    printf ("row130 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 1:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 3 + m] =
                        l[i][27 * (j / 3) + 21 + m] = 1;
#if DEBUG
                    printf ("row131 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 2:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 3 + m] =
                        l[i][27 * (j / 3) + 12 + m] = 1;
#if DEBUG
                    printf ("row132 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                  }
              else if (v1 == 3 && v2 == 3)
                switch (j % 3)
                  {
                  case 0:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 15 + m] =
                        l[i][27 * (j / 3) + 24 + m] = 1;
#if DEBUG
                    printf ("row120 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 1:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 6 + m] =
                        l[i][27 * (j / 3) + 24 + m] = 1;
#if DEBUG
                    printf ("row121 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 2:
                    for (m = 0; m < 3; ++m)
                      l[i][27 * (j / 3) + 6 + m] =
                        l[i][27 * (j / 3) + 15 + m] = 1;
#if DEBUG
                    printf ("row122 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                  }
            }
        }
      // check columns
      for (j = 0; j < 9; ++j)
        {
          for (k = v = 0; k < 9; ++k)
            v += l[i][9 * k + j];
#if DEBUG
          if (v == 8)
            for (k = 0; k < 9; ++k)
              printf ("row=%u col=%u l=%u\n", k + 1, j + 1, l[i][9 * k + j]);
#endif
          if (v == 8)
            for (k = 0; k < 9; ++k)
              if (!l[i][9 * k + j])
                {
#if DEBUG
                  printf ("%u in row %u column %u (col)\n", i, k + 1, j + 1);
#endif
                  stat[9 * k + j] = i;
                  return 1;
                }
          if (v == 6 || v == 7)
            {
              for (k = v1 = 0; k < 3; ++k)
                v1 += l[i][9 * k + j];
              for (v2 = 0; k < 6; ++k)
                v2 += l[i][9 * k + j];
              for (v3 = 0; k < 9; ++k)
                v3 += l[i][9 * k + j];
              if (v2 == 3 && v3 == 3)
                switch (j % 3)
                  {
                  case 0:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 1]
                        = l[i][9 * m + 3 * (j / 3) + 2] = 1;
#if DEBUG
                    printf ("col230 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 1:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3)]
                        = l[i][9 * m + 3 * (j / 3) + 2] = 1;
#if DEBUG
                    printf ("col231 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 2:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3)]
                        = l[i][9 * m + 3 * (j / 3) + 1] = 1;
#if DEBUG
                    printf ("col232 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                  }
              else if (v1 == 3 && v3 == 3)
                switch (j % 3)
                  {
                  case 0:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 28]
                        = l[i][9 * m + 3 * (j / 3) + 29] = 1;
#if DEBUG
                    printf ("col130 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 1:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 27]
                        = l[i][9 * m + 3 * (j / 3) + 29] = 1;
#if DEBUG
                    printf ("col131 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 2:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 27]
                        = l[i][9 * m + 3 * (j / 3) + 28] = 1;
#if DEBUG
                    printf ("col132 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                  }
              else if (v1 == 3 && v2 == 3)
                switch (j % 3)
                  {
                  case 0:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 55]
                        = l[i][9 * m + 3 * (j / 3) + 56] = 1;
#if DEBUG
                    printf ("col120 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 1:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 54]
                        = l[i][9 * m + 3 * (j / 3) + 56] = 1;
#if DEBUG
                    printf ("col121 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                    break;
                  case 2:
                    for (m = 0; m < 3; ++m)
                      l[i][9 * m + 3 * (j / 3) + 54]
                        = l[i][9 * m + 3 * (j / 3) + 55] = 1;
#if DEBUG
                    printf ("col122 %u %u\n", i, j);
                    print (&l[i][0]);
#endif
                  }
            }
        }
      // check boxes 
      for (j = 0; j < 9; ++j)
        {
          for (k = v = 0; k < 3; ++k)
            for (m = 0; m < 3; ++m)
              v += l[i][27 * (j / 3) + 3 * (j % 3) + 9 * k + m];
          if (v == 8)
            for (k = 0; k < 3; ++k)
              for (m = 0; m < 3; ++m)
                if (!l[i][27 * (j / 3) + 3 * (j % 3) + 9 * k + m])
                  {
#if DEBUG
                    printf ("%u in row %u column %u (box)\n",
                            i, (j / 3) + k + 1, j % 3 + m + 1);
#endif
                    stat[27 * (j / 3) + 3 * (j % 3) + 9 * k + m] = i;
                    return 1;
                  }
        }
    }
#if DEBUG
  printf ("s=%u count=%u\n", s, count (l));
#endif
  if (count (l) != s)
    goto bucle;
  return 0;
}

static inline int
solve (unsigned int stat[81], unsigned int lr[9][10], unsigned int lc[9][10],
       unsigned int lb[9][10], int *error_code)
{
  *error_code = init (stat, lr, lc, lb);
  if (*error_code)
    return 0;
  if (discards (stat) || cells (stat, lr, lc, lb))
    return 1;
  return -1;
}

int
play (unsigned int stat[81], int *error_code, unsigned int deep,
      unsigned int *deep_max)
{
  unsigned int s[81], lr[9][10], lc[9][10], lb[9][10];
  unsigned int i, j, r, c, b;
  ++deep;
  if (deep > *deep_max)
    *deep_max = deep;
  if (max_allowed_deep && deep > max_allowed_deep)
    goto exit1;
  memcpy (s, stat, 81 * sizeof (unsigned int));
  while (solve (s, lr, lc, lb, error_code) > 0);
  *error_code = init (s, lr, lc, lb);
  if (*error_code)
    goto exit1;
  if (value (s))
    goto exit0;
  for (i = 0; i < 81; ++i)
    if (!s[i])
      {
        get_index (i, &r, &c, &b);
        for (j = 1; j < 10; ++j)
          if (!lr[r][j] && !lc[c][j] && !lb[b][j])
            {
              s[i] = j;
              if (!play (s, error_code, deep, deep_max))
                goto exit0;
              s[i] = 0;
            }
      }
exit1:
  return 1;
exit0:
  memcpy (stat, s, 81 * sizeof (unsigned int));
  return 0;
}

int
main (int argn, char **argc)
{
  unsigned int lr[9][10], lc[9][10], lb[9][10];
  unsigned int stat[81];
  FILE *file;
  int error_code;
  unsigned int i, deep;

  // check arguments
  switch (argn)
    {
    case 3:
      max_allowed_deep = (unsigned int) atoi (argc[2]);
      break;
    case 2:
      max_allowed_deep = 0;
      break;
    default:
      error_msg = "The syntax is:\n./sudoku input_file [max_deep]";
      error_code = ERROR_CODE_BAD_INPUT_FILE;
      goto exit0;
    }

  // open input file
  file = fopen (argc[1], "r");
  if (!file)
    {
      error_msg = "Unable to open the input file";
      error_code = ERROR_CODE_NO_INPUT_FILE;
      goto exit0;
    }

  // read and check numbers in input file
  for (i = 0; i < 81; ++i)
    if (fscanf (file, "%u", stat + i) != 1 || stat[i] > 9)
      {
        error_msg = "Bad input file";
        error_code = ERROR_CODE_BAD_INPUT_FILE;
        goto exit1;
      }

  // play
  deep = 0;
  if (max_allowed_deep)
    play (stat, &error_code, 0, &deep);
  else
    for (max_allowed_deep = 1;
         max_allowed_deep < 12 && play (stat, &error_code, 0, &deep);
         ++max_allowed_deep)
      deep = 0;

  // check errors
  error_code = init (stat, lr, lc, lb);
  if (error_code)
    goto exit1;
  error_code = ERROR_CODE_NONE;

  // print solution
  print (stat);
  printf ("max deep=%u\n", deep);

  // close file, show message on error and exit
exit1:
  fclose (file);
exit0:
  if (error_code)
    printf ("ERROR!\n%s\n", error_msg);
  return error_code;
}

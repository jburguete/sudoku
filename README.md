sudoku
======

A program to solve sudoku maps

AUTHORS
-------

* Javier Burguete Tolosa
  [jburguete@eead.csic.es](mailto:jburguete@eead.csic.es).

BUILDING THE EXECUTABLES
------------------------

REQUIRED LIBRARIES AND UTILITIES
________________________________

Mandatory:
* [gcc](https://gcc.gnu.org) or [clang](http://clang.llvm.org) to compile the
  source code.
* [make](http://www.gnu.org/software/make) to build the executable file.

OPERATIVE SYSTEMS
_________________

You can install all required utilities and libraries using the instructions of
[install-unix](https://gitlab.com/jburguete/sudoku).

This software has been built and tested in the following operative systems:
* Arch Linux
* Debian 12 (Linux)
* Devuan Linux 4
* Dragonfly BSD 6.4.0
* Fedora Linux 38
* FreeBSD 13.2
* Gentoo Linux
* Linux Mint DE 5
* MacOS Ventura + Homebrew
* Manjaro Linux
* Microsoft Windows 10 + MSYS2
* NetBSD 9.3
* OpenBSD 7.3
* OpenInidiana Hipster
* OpenSUSE Linux 15.5
* Ubuntu Linux 23.04

On Microsoft Windows systems you have to install
[MSYS2](http://sourceforge.net/projects/msys2) and the required
libraries and utilities. You can follow detailed instructions in
[install-unix](https://github.com/jburguete/install-unix/blob/master/tutorial.pdf)
tutorial.

On NetBSD 9.3, to use the last GCC version, you have to do first on the
building terminal:
> $ export PATH="/usr/pkg/gcc9/bin:$PATH"

To do permanent this change the following line can be added to the ".profile"
file in the user root directory:
> PATH="/usr/pkg/gcc9/bin:$PATH"

On OpenBSD 7.3 you have to do first on the building terminal:
> $ export AUTOCONF\_VERSION=2.69 AUTOMAKE\_VERSION=1.16

BUILDING INSTRUCTIONS
_____________________

1. Load the last program version:
> $ git clone https://gitlab.com/jburguete/sudoku

2. Execute the build script
> $ make

EXECUTING
---------

> $ ./sudoku input\_file

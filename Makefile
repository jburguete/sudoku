.PHONY: clean strip

CFLAGS = -c -g -flto -march=native -O3 -Wall -Wextra -Wpedantic
LDFLAGS = -g -flto
DEF = -DALGORITHM=
ifeq ("x$(CC)", "xcc") 
GEN = sudoku.gcda
CFLAGS_GEN = $(CFLAGS) -fprofile-generate
LDFLAGS_GEN = $(LDFLAGS) -fprofile-generate
CFLAGS_USE = $(CFLAGS) -fprofile-use -fprofile-correction
LDFLAGS_USE = $(LDFLAGS) -fprofile-use -fprofile-correction
else
GEN = sudoku.profraw
CFLAGS_GEN = $(CFLAGS) -fprofile-instr-generate
LDFLAGS_GEN = $(LDFLAGS) -fprofile-instr-generate
CFLAGS_USE = $(CFLAGS) -fprofile-instr-use=$(GEN)
LDFLAGS_USE = $(LDFLAGS) -fprofile-instr-use=$(GEN)
PREFIX = LLVM_PROFILE_FILE="sudoku.profdata"
MERGE = llvm-profdata merge sudoku.profdata -output $(GEN)
endif

all: sudoku

sudoku: sudoku.o
	$(CC) $(LDFLAGS_USE) sudoku.o -o sudoku

sudoku.o: $(GEN)
	$(CC) $(CFLAGS_USE) $(DEF) sudoku.c -o sudoku.o

$(GEN): sudokupgo scriptpgo.sh
	$(PREFIX) bash scriptpgo.sh 
	$(MERGE)

sudokupgo: sudoku.pgo
	$(CC) $(LDFLAGS_GEN) sudoku.pgo -o sudokupgo

sudoku.pgo: sudoku.c Makefile
	$(CC) $(CFLAGS_GEN) $(DEF) sudoku.c -o sudoku.pgo

clean:
	rm *pgo *.o sudoku *.gcda *.profdata *.profraw

strip:
	gmake
	strip sudoku
